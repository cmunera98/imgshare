const mongoose = require('mongoose');
const path = require('path');
const {Schema} = mongoose;

const ImageSchema = new Schema({
    title: {type:String},
    description: {type:String},
    filename: {type:String},
    views: {type:Number, default:0},
    likes: {type:Number, default:0},
    timestamp:{type:Date, default: Date.now}

});
// variable virtual. motivo de creacion: en base de datos la imagen se guarda con todo su nombre inclusive la 
// extensión,pero cuando la pidan es decir,cuando haga una consulta por ella no queremos hacer consultas por 
// su extension, solo queremos su id (052fb4e).
ImageSchema.virtual('uniqueId')
    .get(function () {
        return this.filename.replace(path.extname(this.filename), '')
    });

module.exports = mongoose.model('Image',ImageSchema);