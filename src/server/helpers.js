const moment = require('moment');
const helpers = {};

helpers.timeAgo = timestamp => {
    return moment(timestamp).startOf('minute').fromNow();
    // a partir del tiempo que le estoy dando yo quiero que colo en minutos cuanto tiempo ha pasado  
};

module.exports = helpers;