const ctrl = {};
const {Image} = require('../models');

// con la configuracion que se hizo en server/config no es necesaria la extension bhs, el controlador
// ya lo reconocerá,tampoco colocar la ruta del index. pues él ya sabe cual es nuestro layout 
// y en que carpeta está.
ctrl.index = async (req,res) => {
    const images = await Image.find().sort({timestamps: 1});
    res.render('index',{images}); 

};

module.exports = ctrl;