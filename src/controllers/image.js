const path = require('path');
const {randomNumber} = require('../helpers/libs');
const fs = require('fs-extra');
const { Image } = require('../models');   
const ctrl = {};

ctrl.index = async (req,res) => {
    // res.render('image');
    const image = await  Image.findOne({filename: {$regex: req.params.image_id}});
    
    console.log(image);
    res.render('image',{image});
};

ctrl.create = (req,res) => {

    // busqueda del nombre random. por si el nombre es repetido.
    const saveImage = async () => {
        const imgUrl = randomNumber();        
        const images = await Image.find({filename: imgUrl});
            if(images.length > 0){
                // si el nombre es repetivo se vuelve a ejecutar el mismo proceso anterior.
                saveImage()
            }else{
            console.log(imgUrl); 
            const imageTempPath = req.file.path;
            const ext = path.extname(req.file.originalname).toLowerCase();
            const targetPath = path.resolve(`src/public/upload/${imgUrl}${ext}`)
            
            if(ext === '.png' || ext === '.jpg' || ext === '.jpeg' || ext === '.gif'){
                await fs.rename(imageTempPath, targetPath);
                const newImage = new Image({
                title: req.body.title,
                filename: imgUrl + ext,
                description: req.body.description,
            });
                const saveImage = await newImage.save();
                res.redirect('/images/' + imgUrl);
                // res.send('listo');
            }else{
                await fs.unlink(imageTempPath);
                res.status(500).json({error: 'Solo se permiten archivos con el formato png,jpg,jpeg o gif.'});
            }
        }
    };
    saveImage();
};

ctrl.like = (req,res) => {
    
};

ctrl.comment = (req,res) => {
    
};

ctrl.remove = (req,res) => {
    
};




module.exports = ctrl;
